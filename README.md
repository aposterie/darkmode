# dark-mode

Calculate dark mode color.

Based on [chroma.js](https://github.com/gka/chroma.js) under BSD-3-Clause license.

```ts
import { Color, parseCSS } from "https://gitlab.com/aposterie/darkmode/-/raw/master/mod.js"

Color.parse("#566").darken().saturate(1).hex()
```
