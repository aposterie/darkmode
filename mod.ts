/**
 * darkmode.js - MIT License
 * @author proteriax
 * @preserve
 */
const { pow } = Math

function limit(x: number, min = 0, max = 1) {
  return x < min ? min : x > max ? max : x
}

type ColorArray = [number, number, number]

const DEG2RAD = Math.PI / 180
const RAD2DEG = 180 / Math.PI

export class Color {
  #rgb: ColorArray

  constructor(rgb: ColorArray) {
    console.assert(rgb.length === 3 && arguments.length === 1)
    this.#rgb = rgb.map(x => limit(x, 0, 255)) as ColorArray
  }

  static parse(text: string) {
    return new Color(parseCSS(text))
  }

  rgb(round = true): ColorArray {
    if (round) {
      return this.#rgb.slice(0, 3).map(Math.round) as ColorArray
    }
    return this.#rgb.slice(0, 3) as ColorArray
  }

  hex(): string {
    return (
      "#" +
      this.rgb()
        .map(x => (x < 16 ? "0" : "") + x.toString(16))
        .join("")
    )
  }

  darken(amount = 1): Color {
    const lab = rgb2lab(this.#rgb)
    lab[0] -= LAB_CONSTANTS.Kn * amount
    return new Color(lab2rgb(lab))
  }

  brighten(amount = 1): Color {
    return this.darken(-amount)
  }

  saturate(amount = 1): Color {
    const lch = rgb2lch(this.#rgb)
    lch[1] += LAB_CONSTANTS.Kn * amount
    if (lch[1] < 0) {
      lch[1] = 0
    }
    return new Color(lch2rgb(lch))
  }

  desaturate(amount = 1): Color {
    return this.saturate(-amount)
  }
}

const LAB_CONSTANTS = {
  // Corresponds roughly to RGB brighter/darker
  Kn: 18,

  // D65 standard referent
  Xn: 0.95047,
  Yn: 1,
  Zn: 1.08883,

  t0: 0.137931034, // 4 / 29
  t1: 0.206896552, // 6 / 29
  t2: 0.12841855, // 3 * t1 * t1
  t3: 0.008856452, // t1 * t1 * t1
}

function rgb2lab(color: ColorArray): ColorArray {
  const [x, y, z] = rgb2xyz(color)
  const l = 116 * y - 16
  return [l < 0 ? 0 : l, 500 * (x - y), 200 * (y - z)]
}

function xyz_lab(t: number) {
  if (t > LAB_CONSTANTS.t3) {
    return pow(t, 1 / 3)
  }
  return t / LAB_CONSTANTS.t2 + LAB_CONSTANTS.t0
}

function rgb_xyz(r: number) {
  if ((r /= 255) <= 0.04045) {
    return r / 12.92
  }
  return pow((r + 0.055) / 1.055, 2.4)
}

function rgb2xyz([r, g, b]): ColorArray {
  r = rgb_xyz(r)
  g = rgb_xyz(g)
  b = rgb_xyz(b)
  const x = xyz_lab((0.4124564 * r + 0.3575761 * g + 0.1804375 * b) / LAB_CONSTANTS.Xn)
  const y = xyz_lab((0.2126729 * r + 0.7151522 * g + 0.072175 * b) / LAB_CONSTANTS.Yn)
  const z = xyz_lab((0.0193339 * r + 0.119192 * g + 0.9503041 * b) / LAB_CONSTANTS.Zn)
  return [x, y, z]
}

function lab2rgb([l, a, b]: ColorArray): ColorArray {
  let y = (l + 16) / 116
  let x = isNaN(a) ? y : y + a / 500
  let z = isNaN(b) ? y : y - b / 200

  y = LAB_CONSTANTS.Yn * lab_xyz(y)
  x = LAB_CONSTANTS.Xn * lab_xyz(x)
  z = LAB_CONSTANTS.Zn * lab_xyz(z)

  const r = xyz_rgb(3.2404542 * x - 1.5371385 * y - 0.4985314 * z) // D65 -> sRGB
  const g = xyz_rgb(-0.969266 * x + 1.8760108 * y + 0.041556 * z)
  const b2 = xyz_rgb(0.0556434 * x - 0.2040259 * y + 1.0572252 * z)

  return [r, g, b2]
}

function xyz_rgb(r: number) {
  return 255 * (r <= 0.00304 ? 12.92 * r : 1.055 * pow(r, 1 / 2.4) - 0.055)
}

function lab_xyz(t: number) {
  return t > LAB_CONSTANTS.t1 ? t * t * t : LAB_CONSTANTS.t2 * (t - LAB_CONSTANTS.t0)
}

function lab2lch([l, a, b]: ColorArray): ColorArray {
  const c = Math.sqrt(a * a + b * b)
  let h = (Math.atan2(b, a) * RAD2DEG + 360) % 360
  if (Math.round(c * 10000) === 0) {
    h = Number.NaN
  }
  return [l, c, h]
}

function rgb2lch(color: ColorArray) {
  return lab2lch(rgb2lab(color))
}

/**
 * Convert from a qualitative parameter h and a quantitative parameter l to a 24-bit pixel.
 * These formulas were invented by David Dalrymple to obtain maximum contrast without going
 * out of gamut if the parameters are in the range 0-1.
 *
 * A saturation multiplier was added by Gregor Aisch
 */
function lch2lab([l, c, h]: ColorArray): ColorArray {
  if (isNaN(h)) h = 0
  h = h * DEG2RAD
  return [l, Math.cos(h) * c, Math.sin(h) * c]
}

function lch2rgb(color: ColorArray): ColorArray {
  return lab2rgb(lch2lab(color))
}

/**
 * @param string format of rgb(x, y, z)
 */
export function parseCSS(string: string) {
  return string
    .slice(4, -1)
    .split(", ")
    .map(x => parseInt(x, 10)) as ColorArray
}
